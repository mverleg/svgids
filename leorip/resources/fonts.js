var prefsLoaded = false;
var defaultFontSize =12;
var currentFontSize = defaultFontSize;

function revertStyles(){

	currentFontSize = defaultFontSize;
	changeFontSize(0);

}

function toggleColors(){
	if(currentStyle == "White"){
		setColor("Black");
	}else{
		setColor("White");
	}
}

function changeFontSize(sizeDifference){
	currentFontSize = parseInt(currentFontSize) + parseInt(sizeDifference);

	if(currentFontSize > 20){
		currentFontSize = 20;
	}else if(currentFontSize < 8){
		currentFontSize = 8;
	}

	setFontSize(currentFontSize);
};

function setFontSize(fontSize){
	var stObj = (document.getElementById) ? document.getElementById('content_area') : document.all('content_area');
	document.body.style.fontSize = fontSize + 'px';

	//alert (document.body.style.fontSize);
};
