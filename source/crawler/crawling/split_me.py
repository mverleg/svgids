
import urllib2
from bs4 import BeautifulSoup
from django.utils.text import slugify
from hashlib import md5
from crawler.models.crawl_session import CrawlSession


def crawl_all():
    base_url = 'http://www.studiegids.science.ru.nl%s'
    start_url = 'http://www.studiegids.science.ru.nl/2013/en/'
    crawler = CrawlSession()
    
    property_types = dict((ptype.slug, ptype) for ptype in PropertyType.objects.all())
    course_codes = []
    failed = []
    #print property_types.keys()
    
    years = crawl_years(start_url)
    for year_name, year_url, year_soup in years:
        faculties = crawl_faculties(base_url % year_url)
        for faculty_name, faculty_url, faculty_soup in faculties:
            print faculty_url
            studies = crawl_studies(base_url % faculty_url)
            for study_name, study_url, study_soup in studies:
                if study_url.startswith('http'):
                    continue
                print '  ' + base_url % study_url + 'courses/'
                courses = crawl_courses(base_url % study_url + 'courses/')
                for course_name, course_url, course_soup in courses:
                    print '    %s' % course_url
                    properties = course_properties(base_url % course_url)
                    if properties:
                        obj = properties_to_instance(crawler, properties, property_types, course_codes)
                    else:
                        failed.append(course_url)
                    #print properties.keys()
                    #instance = properties_to_instance(properties, property_types = property_types)
    print 'failed:\n%s' % '\n'.join(failed)
    crawler.save()

def crawl_years(url):
    page = soupify(url)
    return links_in_element(page.find('td', {'class': 'zijbalkTekst'}))

def crawl_faculties(url):
    page = soupify(url)
    return links_in_element(page.find('div', {'class': 'menuContainer'}))

def crawl_studies(url):
    page = soupify(url)
    return links_in_element(page.find('div', {'class': 'pageContents'}))

def crawl_courses(url):
    page = soupify(url)
    courses = links_in_element(page.find('div', {'class': 'pageContents'}))
    return [course for course in courses if course[1].startswith('/')]

def course_properties(url):
    page = soupify(url)
    properties = {}
    
    try:
        top_fields = page.find('table', {'class': 'course'}).findAll('td')
        paragraphs = page.find('table', {'class': 'coursetext'}).findAll('td')
        side_fields = page.find('table', {'class': 'course_extra'}).findAll('td')
    except AttributeError, e:
        print 'page not a valid course overview: %s (main table classes not found)' % url
        return {}
    
    properties['name'] = unicode(page.find('h1').text).strip()
    
    for top_field in top_fields:
        attribute = top_field.find('b')
        if top_field.find('b'):
            value = unicode(top_field.findAll('br')[-1].next)
            properties[attribute.text.strip()] = value.strip()
    
    for paragraph in paragraphs:
        bold = paragraph.find('b')
        if not bold:
            print 'page not a valid course overview: %s (content could not be read)' % url
            return {}
        attribute = bold.text
        value = ' '.join(' '.join(unicode(elem).split()) for elem in bold.findAllNext())
        properties[attribute] = value
    
    for side_field in side_fields:
        attribute = side_field.find('b').text
        value = ' '.join(' '.join(unicode(elem).split()) for elem in side_field.find('b').findAllNext())
        properties[attribute] = value
    
    return properties

def properties_to_instance(crawl, properties, property_types, course_codes):
    try:
        properties['course-id']
    except KeyError:
        print 'no course id for "%s"' % properties['name']
        print 'options are: %s' % ', '.join(properties.keys())
        return 
    
    course_code = None
    for name, text in properties.items():
        property_type = property_type_obj(name, property_types)
        if property_type.slug == 'course-id':
            course_code = text
    
    if course_code:
        if course_code in course_codes:
            return
        else:
            course = course_obj(course_code, properties['name'])
        #course_codes.append(course_code)
    else:
        print 'course code not found for %s' % properties['name']
        return
    
    course = course_obj()
    
    
    # compare properties to previous crawled version, only store if changed
    
     
    #CourseProperty(crawl = crawl, user = None, course, title, text)
    

def course_obj(course_code, name):
    try:
        course = Course.objects.get(code = course_code)
    except Course.DoesNotExist:
        lecturers = models.ManyToManyField(Lecturer, blank = True)
        studies = models.ManyToManyField(Study, blank = True)
        specializations = models.ManyToManyField(Specialization, blank = True)
        course = Course(code = course_code, )
    return course

def property_type_obj(name, property_types):
    slug = slugify(name)
    if slug in property_types.keys():
        property = property_types[slug]
    else:
        property = PropertyType(slug = slug, name = name)
        property_types[slug] = property
        property.save()
    return property

def soupify(url):
    html = request_url(url)
    #html = unicode(html.decode('utf-8','ignore'))
    return BeautifulSoup(html)

def request_url(url):
    url_hash = md5(url).hexdigest()
    try:
        with open('/tmp/gids_%s.html' % url_hash, 'r') as fh:
            html = fh.read()
    except IOError:
        html = urllib2.urlopen(url).read()
        with open('/tmp/gids_%s.html' % url_hash, 'w+') as fh:
            fh.write(html)
    return html

def links_in_element(soup):
    return [(anchor.text.strip(), anchor['href'], anchor) for anchor in soup.findAll('a')]








