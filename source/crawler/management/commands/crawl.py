
'''
    crawler study guides
'''

from django.core.management.base import BaseCommand
from crawler.functions import crawl_all



class Command(BaseCommand):
    args = '(there are no options)'
    help = 'crawler Radboud study guides'
    
    def handle(self, *args, **options):
        crawl_all()    


