from django.db import models
from django.contrib.auth.models import User

from course.models.lecturer import Lecturer  # @UnresolvedImport
from crawler.models import CrawlSession


class Course(models.Model):
    code = models.CharField(max_length = 16, unique = True)
    name = models.CharField(max_length = 256)
    lecturers = models.ManyToManyField(Lecturer, blank = True)
    #studies = models.ManyToManyField(Study, blank = True)
    #specializations = models.ManyToManyField(Specialization, blank = True)
    #required = models.ManyToManyField(Course, related_name = 'required_for')
    #advised = models.ManyToManyField(Course, related_name = 'advised_for')
    
    def __unicode__(self):
        return '%s: %s' % (self.code, self.name)
    

class PropertyType(models.Model):
    slug = models.SlugField(unique = True)
    name = models.CharField(max_length = 256)
    
    def __unicode__(self):
        return '$%s' % (self.slug)
    

class CourseProperty(models.Model):
    prop_type = models.ForeignKey(PropertyType)
    crawl = models.ForeignKey(CrawlSession, blank = True)
    user = models.ForeignKey(User, blank = True)
    created = models.DateTimeField(auto_now_add = True)
    course = models.ForeignKey(Course)
    title = models.CharField(max_length = 256)
    text = models.TextField()
    
    @property
    def official(self):
        return self.user is None
    

class PropertyListItem(models.Model):
    property = models.ForeignKey(CourseProperty)
    user = models.ForeignKey(User, blank = True)
    created = models.DateTimeField(auto_now_add = True)
    text = models.TextField()
    
    #@property
    #def official(self):
    #    return self.user is None
    

