from django.db import models

# Create your models here.

class Lecturer(models.Model):
    name = models.CharField(max_length = 256)
    
    def __unicode__(self):
        return '%s' % (self.name)


